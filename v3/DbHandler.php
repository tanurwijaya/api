<?php


class DbHandler {

    private $conn;

    function __construct() {
        require_once dirname(__FILE__) . '/DbConnect.php';
        // opening db connection
        $db = new DbConnect();
        $this->conn = $db->connect();
    }


    // public function getWilayah(){
    //     $stmt = $this->conn->prepare("SELECT *
    //         FROM tb_wilayah
    //         ORDER BY wilayah_id asc");
    //     $stmt->execute();
    //     $tasks = $stmt->get_result();
    //     $stmt->close();
    //     return $tasks;
    // }

    public function getProvinsi(){
        $stmt = $this->conn->prepare("SELECT *
            FROM tb_provinsi WHERE provinsi LIKE '_%'
            ORDER BY provinsi_id asc");
        $stmt->execute();
        $tasks = $stmt->get_result();
        $stmt->close();
        return $tasks;
    }

    public function getKabupaten($provinsi_id){
        $stmt = $this->conn->prepare("SELECT *
            FROM tb_kabupatenkota
            WHERE provinsi_id = '$provinsi_id'
            ORDER BY kabupatenkota_id asc");
        $stmt->execute();
        $tasks = $stmt->get_result();
        $stmt->close();
        return $tasks;
    }

    public function getKecamatan($kabupatenkota_id){
        $stmt = $this->conn->prepare("SELECT *
            FROM tb_kecamatan
            WHERE kabupatenkota_id = '$kabupatenkota_id'
            ORDER BY kecamatan_id asc");
        $stmt->execute();
        $tasks = $stmt->get_result();
        $stmt->close();
        return $tasks;
    }


    // public function getNUTPK($stringNUPTK){
    //     $stmt = $this->conn->prepare("SELECT *
    //         FROM tb_guru
    //         WHERE nuptk like '$stringNUPTK%'
    //         ORDER BY nuptk asc");
    //     $stmt->execute();
    //     $tasks = $stmt->get_result();
    //     $stmt->close();
    //     return $tasks;
    // }

    public function getHintNuptkOrNik($nuptkOrNik){
        $stmt = $this->conn->prepare("SELECT tbg.guru_id, tbg.nuptk, tbg.nik, tbg.tgl_lahir, tbg.nama, tbs.sekolah, tbkk.kabupatenkota, tbp.provinsi  
            FROM tb_guru tbg, tb_sekolah tbs, tb_kabupatenkota tbkk, tb_provinsi tbp 
            WHERE (tbg.nuptk LIKE '$nuptkOrNik%' OR tbg.nik LIKE '$nuptkOrNik%') AND tbg.sekolah_id=tbs.sekolah_id 
            AND tbs.kabupatenkota_id=tbkk.kabupatenkota_id AND tbkk.provinsi_id = tbp.provinsi_id 
            ORDER BY tbg.nuptk");
        $stmt->execute();
        $tasks = $stmt->get_result();
        $stmt->close();
        return $tasks;

        // SELECT tbg.guru_id, tbg.nuptk, tbg.nik, tbg.tgl_lahir, tbg.nama, tbs.sekolah FROM tb_guru tbg, tb_sekolah tbs WHERE (tbg.nuptk LIKE '2%' OR tbg.nik LIKE '2%') AND tbg.sekolah_id=tbs.sekolah_id
    }

    public function getNuptkOrNik($nuptkOrNik){
        $stmt = $this->conn->prepare("SELECT tbg.guru_id, tbg.nuptk, tbg.nik, tbg.tgl_lahir, tbg.nama, tbs.sekolah, tbs.sekolah_id, tbkk.kabupatenkota, tbp.provinsi  
            FROM tb_guru tbg, tb_sekolah tbs, tb_kabupatenkota tbkk, tb_provinsi tbp 
            WHERE (tbg.nuptk = '$nuptkOrNik' OR tbg.nik = '$nuptkOrNik') AND tbg.sekolah_id=tbs.sekolah_id 
            AND tbs.kabupatenkota_id=tbkk.kabupatenkota_id AND tbkk.provinsi_id = tbp.provinsi_id 
            ORDER BY tbg.nuptk");
        $stmt->execute();
        $tasks = $stmt->get_result();
        $stmt->close();
        return $tasks;

        // SELECT tbg.guru_id, tbg.nuptk, tbg.nik, tbg.tgl_lahir, tbg.nama, tbs.sekolah FROM tb_guru tbg, tb_sekolah tbs WHERE (tbg.nuptk LIKE '2%' OR tbg.nik LIKE '2%') AND tbg.sekolah_id=tbs.sekolah_id
    }


    public function getNuptk($nuptkNumber){
        $stmt = $this->conn->prepare("SELECT tbg.guru_id, tbg.nuptk, tbg.nik, tbg.tgl_lahir, tbg.nama, tbs.sekolah, tbs.sekolah_id, tbkk.kabupatenkota,tbkk.kabupatenkota_id, tbp.provinsi  
            FROM tb_guru tbg, tb_sekolah tbs, tb_kabupatenkota tbkk, tb_provinsi tbp 
            WHERE tbg.nuptk = '$nuptkNumber' AND tbg.sekolah_id=tbs.sekolah_id 
            AND tbs.kabupatenkota_id=tbkk.kabupatenkota_id AND tbkk.provinsi_id = tbp.provinsi_id 
            ORDER BY tbg.nuptk");
        $stmt->execute();
        $tasks = $stmt->get_result();
        $stmt->close();
        return $tasks;
    }

    public function getNIK($nikNumber){
        $stmt = $this->conn->prepare("SELECT tbg.guru_id, tbg.nuptk, tbg.nik, tbg.tgl_lahir, tbg.nama, tbs.sekolah, tbs.sekolah_id, tbkk.kabupatenkota,tbkk.kabupatenkota_id, tbp.provinsi  
            FROM tb_guru tbg, tb_sekolah tbs, tb_kabupatenkota tbkk, tb_provinsi tbp 
            WHERE tbg.nik = '$nikNumber' AND tbg.sekolah_id=tbs.sekolah_id 
            AND tbs.kabupatenkota_id=tbkk.kabupatenkota_id AND tbkk.provinsi_id = tbp.provinsi_id 
            ORDER BY tbg.nik");
        $stmt->execute();
        $tasks = $stmt->get_result();
        $stmt->close();
        return $tasks;
    }

    public function getHintNuptk($nuptkNumber){
        $stmt = $this->conn->prepare("SELECT tbg.guru_id, tbg.nuptk, tbg.nik, tbg.tgl_lahir, tbg.nama, tbs.sekolah, tbs.sekolah_id, tbkk.kabupatenkota, tbp.provinsi  
            FROM tb_guru tbg, tb_sekolah tbs, tb_kabupatenkota tbkk, tb_provinsi tbp 
            WHERE tbg.nuptk LIKE '$nuptkNumber%' AND tbg.sekolah_id=tbs.sekolah_id 
            AND tbs.kabupatenkota_id=tbkk.kabupatenkota_id AND tbkk.provinsi_id = tbp.provinsi_id 
            ORDER BY tbg.nuptk");
        $stmt->execute();
        $tasks = $stmt->get_result();
        $stmt->close();
        return $tasks;
    }

    public function getHintNIK($nikNumber){
        $stmt = $this->conn->prepare("SELECT tbg.guru_id, tbg.nuptk, tbg.nik, tbg.tgl_lahir, tbg.nama, tbs.sekolah, tbs.sekolah_id, tbkk.kabupatenkota, tbp.provinsi  
            FROM tb_guru tbg, tb_sekolah tbs, tb_kabupatenkota tbkk, tb_provinsi tbp 
            WHERE tbg.nik LIKE '$nikNumber%' AND tbg.sekolah_id=tbs.sekolah_id 
            AND tbs.kabupatenkota_id=tbkk.kabupatenkota_id AND tbkk.provinsi_id = tbp.provinsi_id 
            ORDER BY tbg.nik");
        $stmt->execute();
        $tasks = $stmt->get_result();
        $stmt->close();
        return $tasks;
    }





    public function getHintSekolah($kabupatenkota_id,$keyword){
        $stmt = $this->conn->prepare("SELECT *
            FROM tb_sekolah
            WHERE kabupatenkota_id = '$kabupatenkota_id' AND sekolah LIKE '%$keyword%'
            ORDER BY sekolah_id asc");
        $stmt->execute();
        $tasks = $stmt->get_result();
        $stmt->close();
        return $tasks;
    }


    public function getQuestions(){
        $stmt = $this->conn->prepare("SELECT a.dimensi_id, a.dimensi, b.indikator_id, b.indikator, c.butir_id, c.butir
            FROM tb_dimensi a, tb_indikator b, tb_butir c
            WHERE a.dimensi_id = b.dimensi_id AND b.indikator_id = c.indikator_id
            ORDER BY a.dimensi_id asc, b.indikator_id asc");
        $stmt->execute();
        $tasks = $stmt->get_result();
        $stmt->close();
        return $tasks;
    }


    public function checkUser($email_guru) {
        $stmt = $this->conn->prepare("SELECT * from tb_transaksi_guru WHERE email_guru = '$email_guru'");
        if ($stmt->execute()) {
            $user = $stmt->get_result()->fetch_assoc();
            $stmt->close();
            return $user;
        } else {
            return NULL;
        }

    }


    public function getResult($email_guru) {
        $stmt = $this->conn->prepare("SELECT * FROM tb_transaksi_kusioner WHERE email_guru = '$email_guru' ORDER BY create_at asc, butir_id asc;");
        $stmt->execute();
        $tasks = $stmt->get_result();
        return $tasks;
    }


    public function getHasilDimensi($email_guru) {
        $stmt = $this->conn->prepare("SELECT * from tb_hasil_dimensi WHERE email_guru = '$email_guru' ORDER BY create_at asc");
        $stmt->execute();
        $tasks = $stmt->get_result();
        return $tasks;
    }


    public function postAnswer($email_guru,$jawaban,$create_at)
    {
        $query = "INSERT INTO tb_transaksi_kusioner(email_guru,butir_id,jawaban) values";
        for ($i=0; $i <28 ; $i++) {

            $query .= "(\"".$email_guru."\"".","."\"".($i+1)."\"".","."\"".substr($jawaban, ($i), 1)."\"".")";
            if ($i!=27) {
                $query .=",";
            }
        }
        // echo $query;
        $stmt = $this->conn->prepare($query);
        $result = $stmt->execute();
        $stmt->close();
        return $result;
    }


    public function transaksiGuru($email_guru,$status_kepegawaian_id,$nuptk_id,$sekolah_id,
        $jenjang_sekolah_id,$jenis_guru_id,$sex_id, $usia,$sertifikasi_id,$pendidikan_id,$lama_mengajar)
    {
        $stmt = $this->conn->prepare("INSERT INTO tb_transaksi_guru(email_guru,status_kepegawaian_id,nuptk_id, sekolah_id, jenjang_sekolah_id, jenis_guru_id, sex_id, usia, sertifikasi_id, pendidikan_id, lama_mengajar) values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
        $stmt->bind_param("sssssssssss", $email_guru, $status_kepegawaian_id, $nuptk_id, $sekolah_id, $jenjang_sekolah_id, $jenis_guru_id, $sex_id, $usia, $sertifikasi_id, $pendidikan_id, $lama_mengajar);
        $result = $stmt->execute();
        $stmt->close();
        return $result;
    }

    

    public function postHasilDimensi($email_guru,$A,$B,$C,$D,$E,$F,$create_at){
        $stmt = $this->conn->prepare("INSERT INTO tb_hasil_dimensi(email_guru,A,B,C,D,E,F) values('$email_guru','$A','$B','$C','$D','$E','$F')");
        // $stmt->bind_param("sssssssssss", $email_guru, $status_kepegawaian_id, $nuptk_id, $sekolah_id, $jenjang_sekolah_id, $jenis_guru_id, $sex_id, $usia, $sertifikasi_id, $pendidikan_id, $lama_mengajar);
        $result = $stmt->execute();
        $stmt->close();
        return $result;
    }


    public function postEngagement($email_guru,$A,$B,$C,$D,$E,$F,$create_at){
        $stmt = $this->conn->prepare("INSERT INTO tb_hasil_engagement(email_guru,A,B,C,D,E,F) values('$email_guru','$A','$B','$C','$D','$E','$F')");
        // $stmt->bind_param("sssssssssss", $email_guru, $status_kepegawaian_id, $nuptk_id, $sekolah_id, $jenjang_sekolah_id, $jenis_guru_id, $sex_id, $usia, $sertifikasi_id, $pendidikan_id, $lama_mengajar);
        $result = $stmt->execute();
        $stmt->close();
        return $result;
    }

    public function daftarProfileGuru($nuptk, $nik, $tgl_lahir, $nama, $sekolah_id)
    {
        $stmt = $this->conn->prepare("INSERT INTO tb_guru(nuptk,nik,tgl_lahir, nama, sekolah_id) values('$nuptk', '$nik', '$tgl_lahir', '$nama', '$sekolah_id')");
        // $stmt->bind_param("sssssssssss", $email_guru, $status_kepegawaian_id, $nuptk_id, $sekolah_id, $jenjang_sekolah_id, $jenis_guru_id, $sex_id, $usia, $sertifikasi_id, $pendidikan_id, $lama_mengajar);
        $result = $stmt->execute();
        $stmt->close();
        return $result;
    }



    public function regisSekolah($kabupatenkota_id,$namaSekolah){
        $stmt = $this->conn->prepare("INSERT INTO tb_sekolah(sekolah,kabupatenkota_id) values('$namaSekolah', '$kabupatenkota_id')");
        // $stmt->bind_param("sssssssssss", $email_guru, $status_kepegawaian_id, $nuptk_id, $sekolah_id, $jenjang_sekolah_id, $jenis_guru_id, $sex_id, $usia, $sertifikasi_id, $pendidikan_id, $lama_mengajar);
        $result = $stmt->execute();

        $sekolah_id = $stmt->insert_id;

        $stmt->close();



        
        return $sekolah_id;
    }


    public function getSekolah($kabupatenkota_id,$namaSekolah){
        $stmt = $this->conn->prepare("SELECT *
            FROM tb_sekolah
            WHERE kabupatenkota_id = '$kabupatenkota_id' AND sekolah = '$namaSekolah'
            ORDER BY sekolah_id asc");
        // $stmt->bind_param("sssssssssss", $email_guru, $status_kepegawaian_id, $nuptk_id, $sekolah_id, $jenjang_sekolah_id, $jenis_guru_id, $sex_id, $usia, $sertifikasi_id, $pendidikan_id, $lama_mengajar);
    

        if ($stmt->execute()) {
            $sekolah = $stmt->get_result()->fetch_assoc();
            $stmt->close();
            return $sekolah;
        } else {
            return NULL;
        }

    }


}

?>