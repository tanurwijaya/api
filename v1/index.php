<?php

require_once '../include/DbHandler.php';
// require_once '../include/PassHash.php';
require '.././libs/Slim/Slim.php';

\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();

// User id from db - Global Variable
$user_id = NULL;

function verifyRequiredParams($required_fields) {
    $error = false;
    $error_fields = "";
    $request_params = array();
    $request_params = $_REQUEST;
    // Handling PUT request params
    if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
        $app = \Slim\Slim::getInstance();
        parse_str($app->request()->getBody(), $request_params);
    }
    
    foreach ($required_fields as $field) {
        if (!isset($request_params[$field]) || strlen(trim($request_params[$field])) <= 0) {
            $error = true;
            $error_fields .= $field . ', ';
        }
    }

    if ($error) {
        // Required field(s) are missing or empty
        // echo error json and stop the app
        $response = array();
        $app = \Slim\Slim::getInstance();
        $response["error"] = true;
        $response["message"] = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
        echoRespnse(400, $response);
        $app->stop();
    }
}


// $app->post('/postJawaban',function() use ($app) {

//     verifyRequiredParams(array('email_guru','answers'));

//     $response = array();
//     $email_guru = $app->request->post('email_guru');
//     $answers = $app->request->post('answers');

//     $db = new DbHandler();

    
//     $trans_id = $db->postAnswer($email_guru, $answers);

//     if ($trans_id != NULL) {
//         $response["error"] = false;
//         $response["message"] = "Task created successfully";
//         $response["create_at"] = $trans_id;
//         // $response["feedback_text"] = $feedback_text;
//     } else {
//         $response["error"] = true;
//         $response["message"] = "Failed to create task. Please try again";
//     }
//     echoRespnse(201, $response);
// });


$app->post('/postJawaban',function() use ($app) {

    verifyRequiredParams(array('email_guru','answers','A','B','C','D','E','F','create_at'));

    $response = array();
    $email_guru = $app->request->post('email_guru');
    $answers = $app->request->post('answers');
    $A = $app->request->post('A');
    $B = $app->request->post('B');
    $C = $app->request->post('C');
    $D = $app->request->post('D');
    $E = $app->request->post('E');
    $F = $app->request->post('F');
    $create_at = $app->request->post('create_at');

    $db = new DbHandler();

    
    $trans_id = $db->postAnswer($email_guru, $answers,$create_at);
    // $dimensi_id = $db->postHasilDimensi($email_guru,$A,$B,$C,$D,$E,$F,$create_at);
    // $engagement_id = $db->postEngagement($email_guru,$A*0.108,$B*0.343,$C*0.199,$D*0.17,$E*0.087,$F*0.092,$create_at);


    if ($trans_id != NULL) {
        $response["error"] = false;
        $response["message"] = "Task created successfully";
        $response["create_at"] = $trans_id;
        // $response["feedback_text"] = $feedback_text;
    } else {
        $response["error"] = true;
        $response["message"] = "Failed to create task. Please try again";
    }
    echoRespnse(201, $response);
});


$app->post('/postHasilDimensi',function() use ($app) {

    verifyRequiredParams(array('email_guru','A','B','C','D','E','F'));

    $response = array();
    $email_guru = $app->request->post('email_guru');
    $A = $app->request->post('A');
    $B = $app->request->post('B');
    $C = $app->request->post('C');
    $D = $app->request->post('D');
    $E = $app->request->post('E');
    $F = $app->request->post('F');
    $db = new DbHandler();

    
    $trans_id = $db->postHasilDimensi($email_guru,$A,$B,$C,$D,$E,$F);

    if ($trans_id != NULL) {
        $response["error"] = false;
        $response["message"] = "Data berhasil tersimpan";
    } else {
        $response["error"] = true;
        $response["message"] = "Failed to input score. Please try again";
    }
    echoRespnse(201, $response);
});




$app->post('/registerGuru',function() use ($app) {
    verifyRequiredParams(array('email_guru',
        'status_kepegawaian_id','nuptk_id',
        'sekolah_id','jenjang_sekolah_id','jenis_guru_id','sex_id','usia',
        'sertifikasi_id','pendidikan_id',
        'lama_mengajar'));

    $response = array();

    $email_guru = $app->request->post('email_guru');

    $status_kepegawaian_id = $app->request->post('status_kepegawaian_id');
    $nuptk_id = $app->request->post('nuptk_id');

    $sekolah_id = $app->request->post('sekolah_id');
    $jenjang_sekolah_id = $app->request->post('jenjang_sekolah_id');
    $jenis_guru_id = $app->request->post('jenis_guru_id');
    $sex_id = $app->request->post('sex_id');
    $usia = $app->request->post('usia');

    $sertifikasi_id = $app->request->post('sertifikasi_id');
    $pendidikan_id = $app->request->post('pendidikan_id');

    $lama_mengajar = $app->request->post('lama_mengajar');

    $db = new DbHandler();

    $trans_id = $db->transaksiGuru($email_guru, $status_kepegawaian_id, $nuptk_id, $sekolah_id, $jenjang_sekolah_id, $jenis_guru_id, $sex_id, $usia, $sertifikasi_id, $pendidikan_id, $lama_mengajar);

    if ($trans_id != NULL) {
        $response["error"] = false;
        $response["message"] = "Transaksi Berhasil";
    } else {
        $response["error"] = true;
        $response["message"] = "Gagal menambahkan. Coba lagi.";
    }
    echoRespnse(201, $response);
});


$app->get('/allQuestions',function() {

    $response = array();
    $db = new DbHandler();


    $result = $db->getQuestions();

    $response["error"] = false;
    $response["questions"] = array();

    while ($question = $result->fetch_assoc()) {
        $tmp = array();
        $tmp["dimensi_id"] = $question["dimensi_id"];
        $tmp["dimensi"] = $question["dimensi"];
        $tmp["indikator_id"] = $question["indikator_id"];
        $tmp["indikator"] = $question["indikator"];
        $tmp["butir_id"] = $question["butir_id"];
        $tmp["butir"] = $question["butir"];
        array_push($response["questions"], $tmp);
    }

    echoRespnse(200, $response);
});






// $app->get('/userResult/:email_guru', function($email_guru) {
//     $response = array();
//     $db = new DbHandler();


//     $result = $db->getResult($email_guru);
//     $response["email_guru"] = $result->fetch_assoc()["email_guru"];
//     $grouped = array();
//     while ($question = $result->fetch_assoc()) {

//         if (!array_key_exists($question['create_at'], $grouped)) {
//             $newObject = new stdClass();
//             $newObject->date = $question['create_at'];
//             $newObject->dimensi = array();
//             $newObject->dimensi["a"] = $question["A"];
//             $newObject->dimensi["b"] = $question["B"];
//             $newObject->dimensi["c"] = $question["C"];
//             $newObject->dimensi["d"] = $question["D"];
//             $newObject->dimensi["e"] = $question["E"];
//             $newObject->dimensi["f"] = $question["F"];

//             $newObject->indikator = array();

//             $grouped[$question['create_at']] = $newObject;
//         }

//         $taskObject = new stdClass();
//         $taskObject->butir_id = $question['butir_id'];
//         $taskObject->answer = $question['jawaban'];

//         $grouped[$question['create_at']]->indikator[] = $taskObject;

//     }
    
//     $grouped = array_values($grouped);
//     $response["scores"] = $grouped;

//     echoRespnse(200, $response);
// });


// $app->get('/userResult/:email_guru', function($email_guru) {
//     $response = array();
//     $db = new DbHandler();

//             // fetch task
//     $result = $db->getResult($email_guru);

//     $response["error"] = false;
//     $response["email_guru"] = $result->fetch_assoc()["email_guru"];
//     $response["Total"] = $result->num_rows/28;
//     $response["scores"] = array();
//     $grouped = array();

    

//     while ($question = $result->fetch_assoc()) {
//         $tmp = array();
//         $tmp["butir_id"] = $question["butir_id"];
//         $tmp["jawaban"] = $question["jawaban"];
//         $tmp["create_at"] = $question["create_at"];
//         array_push($response["scores"], $tmp);
//     }


//     foreach ($response["scores"] as $score) {
//         if (!array_key_exists($score['create_at'], $grouped)) {
//             $newObject = new stdClass();
//             $newObject->date = $score['create_at'];
//             $newObject->score = array();
//             $grouped[$score['create_at']] = $newObject;
//         }

//         $taskObject = new stdClass();
//         $taskObject->butir_id = $score['butir_id'];
//         $taskObject->answer = $score['jawaban'];

    
//         $grouped[$score['create_at']]->score[] = $taskObject;
//     }
//     $grouped = array_values($grouped);

//     echoRespnse(200, $grouped);
// });


$app->get('/userResult/:email_guru', function($email_guru) {
    $response = array();
    $db = new DbHandler();


    $result = $db->getResult($email_guru);
    // $response["email_guru"] = $result->fetch_assoc()["email_guru"];
    $grouped = array();
    while ($question = $result->fetch_assoc()) {

        if (!array_key_exists($question['create_at'], $grouped)) {
            $newObject = new stdClass();
            $newObject->date = $question['create_at'];

            $grouped[$question['create_at']] = $newObject;
        }

        $taskObject = new stdClass();
        $taskObject->butir_id = $question['butir_id'];
        $taskObject->answer = $question['jawaban'];

        $grouped[$question['create_at']]->score[] = $taskObject;

    }
    
    $grouped = array_values($grouped);
    $response["scores"] = $grouped;

    echoRespnse(200, $grouped);
});






$app->post('/checkLogin',function() use ($app) {

    verifyRequiredParams(array('email_guru'));

    $response = array();
    $email_guru = $app->request->post('email_guru');
    $db = new DbHandler();

    
    $trans_id = $db->checkUser($email_guru);

    if ($trans_id != NULL) {
        $response["error"] = false;
        $response["message"] = "User Terdaftar";
        $response["create_at"] = $result["create_at"];
        $response["email_guru"] = $result["email_guru"];
        // $response["feedback_text"] = $feedback_text;
    } else {
        $response["error"] = true;
        $response["message"] = "User tidak terdaftar";
    }
    echoRespnse(201, $response);
});

$app->get('/getWilayah',function() {

    $response = array();
    $db = new DbHandler();


    $result = $db->getWilayah();

    
    $response["wilayah"] = array();

    while ($wilayah = $result->fetch_assoc()) {
        $tmp = array();
        $tmp["wilayah_id"] = $wilayah["wilayah_id"];
        $tmp["wilayah"] = $wilayah["wilayah"];
        array_push($response["wilayah"], $tmp);
    }

    echoRespnse(200, $response["wilayah"]);
});


$app->get('/getProvinsi/:wilayah_id', function($wilayah_id) {

    $response = array();
    $db = new DbHandler();


    $result = $db->getProvinsi($wilayah_id);

    $response["error"] = false;
    $response["provinsi"] = array();

    while ($provinsi = $result->fetch_assoc()) {
        $tmp = array();
        $tmp["provinsi_id"] = $provinsi["provinsi_id"];
        $tmp["provinsi"] = $provinsi["provinsi"];
        $tmp["wilayah_id"] = $provinsi["wilayah_id"];
        array_push($response["provinsi"], $tmp);
    }

    echoRespnse(200, $response["provinsi"]);
});


$app->get('/getKabupaten/:provinsi_id', function($provinsi_id) {

    $response = array();
    $db = new DbHandler();


    $result = $db->getKabupaten($provinsi_id);

    $response["error"] = false;
    $response["kabupaten"] = array();

    while ($kabupaten = $result->fetch_assoc()) {
        $tmp = array();
        $tmp["kabupatenkota_id"] = $kabupaten["kabupatenkota_id"];
        $tmp["kabupatenkota"] = $kabupaten["kabupatenkota"];
        $tmp["provinsi_id"] = $kabupaten["provinsi_id"];
        array_push($response["kabupaten"], $tmp);
    }

    echoRespnse(200, $response["kabupaten"]);
});


$app->get('/getSekolah/:kabupatenkota_id', function($kabupatenkota_id) {

    $response = array();
    $db = new DbHandler();


    $result = $db->getSekolah($kabupatenkota_id);

    $response["error"] = false;
    $response["sekolah"] = array();

    while ($sekolah = $result->fetch_assoc()) {
        $tmp = array();
        $tmp["sekolah_id"] = $sekolah["sekolah_id"];
        $tmp["sekolah"] = $sekolah["sekolah"];
        $tmp["kabupatenkota_id"] = $sekolah["kabupatenkota_id"];
        array_push($response["sekolah"], $tmp);
    }

    echoRespnse(200, $response["sekolah"]);
});

function echoRespnse($status_code, $response) {
    $app = \Slim\Slim::getInstance();
    // Http response code
    $app->status($status_code);

    // setting response content type to json
    $app->contentType('application/json');

    echo json_encode($response);
}

$app->run();
?>